#pragma once


struct Component
{
	Component();

	int id;

	enum Slots
	{
		UP,
		DOWN,
		LEFT,
		RIGHT,

		NUM_SLOTS
	};
	static std::map<std::string, Slots> slotNames;
	int slots[NUM_SLOTS];

	// TODO: Use only indices of category/type
	enum Category
	{
		WEAPON
	} category;
	static std::map<std::string, Category> categoryNames;
	int type;

	static std::map<std::string, int> typeNames;

	// Degrees
	int orientation;

	// Components are 32x32 squares(for now)
	static const sf::Vector2f size;
};