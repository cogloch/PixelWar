#pragma once
#include "Ship.h"


class ShipController
{
public:
	ShipController(Ship&);
	void Update(float dt);

private:
	Ship& m_ship;
};