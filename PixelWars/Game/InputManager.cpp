#include "InputManager.h"
#include "Events\KeyboardEv.h"


std::map<sf::Keyboard::Key, InputManager::GameplayKeys> InputManager::keyCodeToGameKey = {
	{ sf::Keyboard::W, W },
	{ sf::Keyboard::A, A },
	{ sf::Keyboard::S, S },
	{ sf::Keyboard::D, D }
};

InputManager::InputManager()
{
	RegisterForEvent(IEvent::KEY_UP);
	RegisterForEvent(IEvent::KEY_DOWN);
	RegisterForEvent(IEvent::FRAME);

	for (auto& keyDown : isKeyDown)
		keyDown = false;
}

void InputManager::HandleEvent(IEvent* pEvent, IEvent::EventType type)
{
	switch (type)
	{
	case IEvent::FRAME:
	{
		m_timer.Update();
		float dt = m_timer.Elapsed();
		g_logger.Write("Input Manager dt: " + std::to_string(dt));
		for (auto& ship : m_shipControllers)
			ship.Update(dt);
		break;
	}
	case IEvent::KEY_UP:
		isKeyDown[keyCodeToGameKey[dynamic_cast<KeyUpEv*>(pEvent)->keyCode]] = false;
		break;
	case IEvent::KEY_DOWN:
		isKeyDown[keyCodeToGameKey[dynamic_cast<KeyDownEv*>(pEvent)->keyCode]] = true;
		break;
	}
}

void InputManager::RegisterShipController(Ship& ship)
{
	m_shipControllers.push_back(ship);
}