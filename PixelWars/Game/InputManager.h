#pragma once
#include "Ship.h"
#include "ShipController.h"
#include "Events\IEventListener.h"
#include "Application\Timer.h"


class InputManager : public IEventListener
{
public:
	InputManager();
	void HandleEvent(IEvent*, IEvent::EventType);

	enum GameplayKeys
	{
		W,
		A,
		S,
		D,

		NUM_GAME_KEYS
	};

	enum SystemKeys
	{
		R,

		NUM_SYS_KEYS
	};

	bool isKeyDown[NUM_GAME_KEYS];
	static std::map<sf::Keyboard::Key, GameplayKeys> keyCodeToGameKey;

	void RegisterShipController(Ship&);

private:
	std::vector<ShipController> m_shipControllers;
	Timer m_timer;
};

extern InputManager g_inputManager;