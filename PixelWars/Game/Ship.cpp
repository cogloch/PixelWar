#include "Ship.h"
#include "Resources\FileManager.h"
#include "Rendering\Renderable.h"
#include "Resources\ResourceManager.h"


std::vector<std::string> Ship::shipConfigs = { };


bool Ship::BuildFromData(const std::string& shipData)
{
	// -property = "value"
	//
	// One of:
	// -position	= "floatxfloat"					| e.g.: "120x532"
	//
	// [Number of components] tuples:
	// -id		    = "uint"						| 0 is mandatory
	// -socket      = "parentId.socket"				| UP/DOW/LEFT/RIGHT
	// -category    = "string"						| Available categories in Components.cuc
	// -type        = "string"						| Available types in Components.cuc
	// -orientation = "uint"						| 0/90/180/270
	// -control     = "string"						| Keyboard key

	enum
	{
		POSITION = 1,
		COMPONENT_COUNT = 2,
		BOUNDING_BOX = 3
	};

	static std::regex exprPos(R"delim((?:-position = ")(\d+x\d+)"(?:-component count = ")(\d+)"(?:-bounding box = ")(\d+x\d+ \d+x\d+))delim");
	auto iter = std::sregex_iterator(shipData.begin(), shipData.end(), exprPos);
	if (iter != std::sregex_iterator())
	{
		std::string concatPosition = iter->operator[](POSITION).str();
		size_t separator = concatPosition.find("x");
		position = { 
			std::stof(concatPosition.substr(0, separator)), 
			std::stof(concatPosition.substr(separator + 1)) 
		};

		components.resize(std::stoi(iter->operator[](COMPONENT_COUNT).str()));
	
		// TODO: regex
		std::string bBoxStr = iter->operator[](BOUNDING_BOX).str();
		size_t minMaxSeparator = bBoxStr.find(' ');
		std::string minStr = bBoxStr.substr(0, minMaxSeparator),
					maxStr = bBoxStr.substr(minMaxSeparator + 1);
		
		size_t vecSeparator = minStr.find('x');
		boundingBox.min = {
			std::stof(minStr.substr(0, vecSeparator)),
			std::stof(minStr.substr(vecSeparator + 1))
		};

		vecSeparator = maxStr.find('x');
		boundingBox.max = {
			std::stof(maxStr.substr(0, vecSeparator)),
			std::stof(maxStr.substr(vecSeparator + 1))
		};
	}
	else
	{
		return false;
	}

	// Ayy lmao. I'm ashamed.
	/*static std::regex exprComponent(R"(
		(?:-id = ")(\d+)\"\n
		(? : -socket = ")(\d+\.?\w*)\"\n
		(? : -category = ")(\w+)\"\n
		(? : -type = ")(\w+)\"\n
		(? : -orientation = ")(\d+)\"\n
		(? : -control = ")(\w+)\"
	)");*/
	//std::string chapter1 = R"delim((?:-id = ")(\d+)"\n(?:-socket = ")(\d+\.?\w*)"\n(?:-category = ")(\w+)"\n(?:-type = ")(\w+)"\n)delim";
	//std::string chapter2 = R"delim((?:-orientation = ")(\d+)"\n(?:-control = ")(\w+)")delim";
	//std::string compExpr = chapter1 + chapter2;
	//std::string compExpr(R"delim((?:-id = ")(\d+)")delim");
	std::string compExpr(R"delim((?:-id = ")(\d+)"(?:-socket = ")(\d*\.?\w*)"(?:-category = ")(\w+)"(?:-type = ")(\w+)"(?:-orientation = ")(\d+)"(?:-control = ")(\w+)")delim");
	static std::regex exprComponent(compExpr);

	enum
	{
		INVALID,
		
		ID,
		SOCKET,
		CATEGORY,
		TYPE,
		ORIENTATION,
		CONTROL,

		NUM_ATTRIBUTES
	};

	auto iter2 = std::sregex_iterator(shipData.begin(), shipData.end(), exprComponent);
	for (auto end = std::sregex_iterator(); iter2 != end; iter2++)
	{
		auto values = *iter2;

		int id = std::stoi(values[ID].str());
		components[id].id = id;
		components[id].category = Component::categoryNames[values[CATEGORY].str()];
		components[id].type = g_resources.GetTextureByCompType(values[TYPE].str());
		components[id].orientation = std::stoi(values[ORIENTATION].str());
		
		std::string socketStr = values[SOCKET];
		// If not root
		if (socketStr != "root")
		{
			size_t separator = socketStr.find(".");

			int parent = std::stoi(socketStr.substr(0, separator));
			Component::Slots slot = Component::slotNames[socketStr.substr(separator + 1)];
			components[parent].slots[slot] = id;
		}
	}

	//ComputeBoundingBox();

	return true;
}

bool Ship::BuildFromFile(const std::string& filename)
{
	std::ifstream file(FileManager::GetShipsDirectory() + filename);
	if (file.fail())
		return false;

	std::string shipData;
	std::string line;
	while (std::getline(file, line))
		shipData += line;

	return BuildFromData(shipData);
}

// Maintain at all times min and max
void Ship::ComputeBoundingBox()
{
	std::vector<int> queue(components.size());
	int front = 0, 
		back = 0;
	
	std::vector<sf::Vector2f> compPosition(components.size());
	compPosition[0] = position;

	std::vector<bool> visited(components.size());
	for (auto& componentStatus : visited)
		componentStatus = false;
	
	queue[0] = 0;
	visited[0] = true;

	static float componentSize = Magnitude(Component::size);
	static sf::Vector2f displacements[] = {
		{ 0.0f, -Component::size.y },
		{ 0.0f, Component::size.y },
		{ -Component::size.x, 0.0f },
		{ Component::size.x, 0.0f }
	};

	while (front <= back)
	{
		int current = queue[front++];

		for (int slot = 0; slot < Component::NUM_SLOTS; slot++)
		{
			int consideredComp = components[current].slots[slot];
			if (consideredComp != -1)
			{
				if (!visited[consideredComp])
				{
					compPosition[consideredComp] = compPosition[current] + displacements[slot];
					float currentDistance = MagnitudeSqr(compPosition[consideredComp] - position);
					
					sf::Vector2f posVector = compPosition[consideredComp] - position;
					float posVectorMagSqr = MagnitudeSqr(posVector);
					sf::Vector2f posVectorUnit = posVector / sqrt(posVectorMagSqr);

					static sf::Vector2f horizontalVersor(1.0f, 0.0f);
					if (posVector.x * horizontalVersor.x + posVector.y * horizontalVersor.y < 0)
					{
						if (MagnitudeSqr(boundingBox.max - position) < posVectorMagSqr)
							boundingBox.max = posVector;
					}
					else
					{
						if (MagnitudeSqr(boundingBox.min - position) < posVectorMagSqr)
							boundingBox.min = posVector;
					}

					visited[consideredComp] = true;
					queue[++back] = consideredComp;
				}
			}
		}
	}

	/*for (auto& pos : compPosition)
	{
		float distanceToCenter = MagnitudeSqr(pos - this->position);
		if (distanceToCenter < MagnitudeSqr(boundingBox.min - this->position))

	}*/

	//float currentDistance = MagnitudeSqr(currentPosition - position),
	//	minDistance = MagnitudeSqr(boundingBox.min - position),
	//	maxDistance = MagnitudeSqr(boundingBox.max - position);

	//if (currentDistance < minDistance)
	//	boundingBox.min = currentPosition;        
	//if (currentDistance > maxDistance)
	//	boundingBox.max = currentPosition;
}

std::vector<Renderable> Ship::GetRenderables()
{
	std::vector<Renderable> renderables;
	renderables.reserve(components.size());

	std::vector<int> queue(components.size());
	int front = 0,
		back = 0;

	queue[0] = 0;

	std::vector<sf::Vector2f> positions(components.size());
	positions[0] = position;

	std::vector<bool> visited(components.size());
	for (auto& visitStatus : visited)
		visitStatus = false;
	visited[0] = true;

	static float componentSize = Magnitude(Component::size);
	static sf::Vector2f displacements[] = {
		{ 0.0f, -Component::size.y },
		{ 0.0f, Component::size.y },
		{ -Component::size.x, 0.0f },
		{ Component::size.x, 0.0f }
	};

	while (front <= back)
	{
		int current = queue[front++];
		renderables.push_back({ components[current].type, positions[current] });	
	
		for (int slot = 0; slot < Component::NUM_SLOTS; slot++)
		{
			int consideredComp = components[current].slots[slot];
			if (consideredComp != -1)
			{
				if (!visited[consideredComp])
				{
					positions[consideredComp] = positions[current] + displacements[slot];
					visited[consideredComp] = true;
					queue[++back] = consideredComp;
				}
			}
		}
	}

	return std::move(renderables);
}