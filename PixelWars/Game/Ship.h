#pragma once
#include "Physics\AABB.h"
#include "Component.h"
#include "Rendering\Renderable.h"


class Ship
{
public:
	bool BuildFromFile(const std::string&);
	bool BuildFromData(const std::string&);

	std::vector<Renderable> GetRenderables();

	// A bounding box encompassing all the individual components' bounding boxes. Used for visibiliy tesing and the 
	// first pass in collision detection.
	AABB boundingBox;

	// Position of the component with id = 0; all other components recursively stick to it, making a pretty flower of doom
	sf::Vector2f position;
	std::vector<Component> components;

	static std::vector<std::string> shipConfigs;

	void ComputeBoundingBox();
};