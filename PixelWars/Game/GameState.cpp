#include "GameState.h"
#include "InputManager.h"
#include "Events\GameSessionEv.h"


GameState::GameState() : m_inProgress(false)
{
	RegisterForEvent(IEvent::GAME_START);
	RegisterForEvent(IEvent::GAME_OVER);
	RegisterForEvent(IEvent::FRAME);
}

void GameState::Init(int shipConfig)
{
	// TODO: Move ship configurations out of Ship:: i.e. also this
	Ship::shipConfigs.push_back("SimpleShip.cuc");

	m_camera.SetEnable(true);
	m_camera.view.setSize(800, 600);
	m_camera.view.setCenter(0, 0);
	m_camera.boundingBox = { { -400, -300 }, { 400, 300 } };

	Ship testShip;
	testShip.BuildFromFile(Ship::shipConfigs[shipConfig]);
	m_ships.push_back(testShip);
	g_inputManager.RegisterShipController(m_ships[0]);
}

void GameState::Init(const std::string& sState, int shipConfig)
{

}

void GameState::Close()
{

}

void GameState::Serialize(std::string&)
{

}

void GameState::Update()
{
	m_timer.Update();
	g_logger.Write("Game state dt: " + std::to_string(m_timer.Elapsed()));
}

void GameState::HandleEvent(IEvent* pEvent, IEvent::EventType type)
{
	switch (type)
	{
	case IEvent::GAME_START:
	{
		if (m_inProgress)
			break;
		m_inProgress = true;

		GameStartEv* gameStartEv = dynamic_cast<GameStartEv*>(pEvent);
		std::string serializedState = gameStartEv->state;
		if (!serializedState.length())
			Init(0);
		else
			Init(serializedState, 0);
		break;
	}

	case IEvent::GAME_OVER:
		if (!m_inProgress)
			break;
		Close();
		break;

	case IEvent::FRAME:
		Update();
		break;
	}
}