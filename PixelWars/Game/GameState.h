#pragma once
#include "Ship.h"
#include "Rendering\Camera.h"
#include "Application\Timer.h"
#include "Events\IEventListener.h"


class GameState : public IEventListener
{
public:
	GameState();

	void Init(int);
	void Init(const std::string&, int);
	void Close();

	void Serialize(std::string&);

	void Update();
	void HandleEvent(IEvent*, IEvent::EventType);

	Camera			  m_camera;
	std::vector<Ship> m_ships;

private:
	bool m_inProgress;
	Timer m_timer;
};