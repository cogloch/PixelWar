#include "Component.h"


std::map<std::string, Component::Slots> Component::slotNames = {
	{ "UP", UP },
	{ "DOWN", DOWN },
	{ "LEFT", LEFT },
	{ "RIGHT", RIGHT }
};

std::map<std::string, Component::Category> Component::categoryNames = {
	{ "Weapon", Category::WEAPON }
};

std::map<std::string, int> Component::typeNames = { };

const sf::Vector2f Component::size = { 32.0f, 32.0f };


Component::Component() :
	id(0)
{
	for (auto& slot : slots)
		slot = -1;
}