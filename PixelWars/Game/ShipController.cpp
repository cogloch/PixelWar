#include "ShipController.h"
#include "InputManager.h"


ShipController::ShipController(Ship& ship) : m_ship(ship)
{
}

void ShipController::Update(float dt)
{
	static sf::Vector2f velocityUp(0.0f, -32.0f),
						velocityLeft(-32.0f, 0.0f),
						velocityDown(0.0f, 32.0f),
						velocityRight(32.0f, 0.0f);

	if (g_inputManager.isKeyDown[InputManager::W])
		m_ship.position += velocityUp * dt;
	if (g_inputManager.isKeyDown[InputManager::A])
		m_ship.position += velocityLeft * dt;
	if (g_inputManager.isKeyDown[InputManager::S])
		m_ship.position += velocityDown * dt;
	if (g_inputManager.isKeyDown[InputManager::D])
		m_ship.position += velocityRight * dt;
}