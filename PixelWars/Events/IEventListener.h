#pragma once
#include "IEvent.h"


class IEventListener
{
public:
	virtual ~IEventListener();
	virtual void HandleEvent(IEvent*, IEvent::EventType) = 0;

protected:
	void RegisterForEvent(IEvent::EventType);
};