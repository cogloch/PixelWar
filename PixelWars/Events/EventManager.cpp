#include "EventManager.h"


EventManager::EventManager()
{
}

EventManager::~EventManager()
{
	Close();
}

void EventManager::Close()
{
	// TODO: Lock main queue and clear it too.
	for (auto& pEvent : m_eventQueues[1])
		if (pEvent)
			delete pEvent;

	m_eventQueues[1].clear();

	//for (auto& queue : m_eventQueues)
	//	if (queue.size())
	//	{
	//		for (auto& pEvent : queue)
	//		{
	//			g_logger.Write("Deleting " + std::to_string(pEvent->GetType()));
	//			if (pEvent)
	//				delete pEvent;
	//		}

	//		queue.clear();
	//	}
}

void EventManager::RegisterListener(IEvent::EventType eventType, IEventListener* pListener)
{
	m_eventListeners[eventType].push_back(pListener);
}

void EventManager::RemoveListener(IEventListener* pListener)
{
	for (auto& eventType : m_eventListeners)
		for (unsigned int idx = 0; idx < eventType.size(); idx++)
			if (pListener == eventType[idx])
				eventType.erase(eventType.begin() + idx);
}

enum EventQueueChannel
{
	MAIN_Q,
	SECOND_Q
};

void EventManager::PostEvent(IEvent* pEvent)
{
	m_eventQueues[SECOND_Q].push_back(pEvent);
}

void EventManager::TriggerEvent(IEvent* pEvent)
{
	IEvent::EventType type = pEvent->GetType();
	for (auto& pListener : m_eventListeners[type])
		pListener->HandleEvent(pEvent, type);

	delete pEvent;
}

void EventManager::DispatchEvents()
{
	m_eventQueues[MAIN_Q].clear();
	m_eventQueues[MAIN_Q] = std::move(m_eventQueues[SECOND_Q]);

	for (auto& pEvent : m_eventQueues[MAIN_Q])
	{
		IEvent::EventType type = pEvent->GetType();
		for (auto& pListener : m_eventListeners[type])
			pListener->HandleEvent(pEvent, type);

		delete pEvent;
	}
}