#pragma once
#include "IEvent.h"


class MouseEv : public IEvent
{
public:
	sf::Vector2f position;

protected:
	MouseEv(sf::Vector2f);
};

class LMouseUpEv : public MouseEv
{
public:
	LMouseUpEv(sf::Vector2f);
	EventType GetType();
};

class LMouseDownEv : public MouseEv
{
public:
	LMouseDownEv(sf::Vector2f);
	EventType GetType();
};

class RMouseUpEv : public MouseEv
{
public:
	RMouseUpEv(sf::Vector2f);
	EventType GetType();
};

class RMouseDownEv : public MouseEv
{
public:
	RMouseDownEv(sf::Vector2f);
	EventType GetType();
};