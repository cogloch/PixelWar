#pragma once
#include "IEvent.h"


class NetJoinEv : public IEvent
{
public:
	NetJoinEv();
	EventType GetType();

	std::string ip;
	int port;
};