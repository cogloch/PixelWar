#include "GameSessionEv.h"


GameStartEv::GameStartEv() : state("")
{
}

GameStartEv::GameStartEv(const std::string& state_) : state(state_)
{
}

IEvent::EventType GameStartEv::GetType()
{
	return IEvent::GAME_START;
}

GameOverEv::GameOverEv()
{
}

IEvent::EventType GameOverEv::GetType()
{
	return IEvent::GAME_OVER;
}