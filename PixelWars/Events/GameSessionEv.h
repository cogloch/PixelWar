#pragma once
#include "IEvent.h"


class GameStartEv : public IEvent
{
public:
	GameStartEv();
	GameStartEv(const std::string&);
	IEvent::EventType GetType();
	
	std::string state;
};

class GameOverEv : public IEvent
{
public:
	GameOverEv();
	IEvent::EventType GetType();
};