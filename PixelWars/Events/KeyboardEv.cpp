#include "KeyboardEv.h"


KeyboardEv::KeyboardEv(sf::Keyboard::Key key) : keyCode(key)
{
}

KeyUpEv::KeyUpEv(sf::Keyboard::Key key) : KeyboardEv(key)
{
}

KeyDownEv::KeyDownEv(sf::Keyboard::Key key) : KeyboardEv(key)
{
}

IEvent::EventType KeyUpEv::GetType()
{
	return IEvent::KEY_UP;
}

IEvent::EventType KeyDownEv::GetType()
{
	return IEvent::KEY_DOWN;
}