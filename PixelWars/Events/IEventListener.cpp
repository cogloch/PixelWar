#include "EventManager.h"
#include "IEventListener.h"


IEventListener::~IEventListener()
{
	g_eventManager.RemoveListener(this);
}

void IEventListener::RegisterForEvent(IEvent::EventType type)
{
	g_eventManager.RegisterListener(type, this);
}