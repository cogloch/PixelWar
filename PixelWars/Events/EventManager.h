#pragma once 
#include "IEvent.h"
#include "IEventListener.h"


class EventManager
{
public:
	EventManager();
	~EventManager();

	void Close();

	void RegisterListener(IEvent::EventType, IEventListener*);
	void RemoveListener(IEventListener*);

	void PostEvent(IEvent*);
	void TriggerEvent(IEvent*);

	void DispatchEvents();

private:
	std::vector<IEvent*> m_eventQueues[2];
	std::vector<IEventListener*> m_eventListeners[IEvent::NUM_EVENTS];
};

extern EventManager g_eventManager;