#include "MouseEv.h"


MouseEv::MouseEv(sf::Vector2f position_) : position(position_)
{
}

LMouseUpEv::LMouseUpEv(sf::Vector2f position) : MouseEv(position)
{
}

LMouseDownEv::LMouseDownEv(sf::Vector2f position) : MouseEv(position)
{
}

RMouseUpEv::RMouseUpEv(sf::Vector2f position) : MouseEv(position)
{
}

RMouseDownEv::RMouseDownEv(sf::Vector2f position) : MouseEv(position)
{
}

IEvent::EventType LMouseUpEv::GetType()
{
	return IEvent::MOUSE_LBUTTON_UP;
}

IEvent::EventType LMouseDownEv::GetType()
{
	return IEvent::MOUSE_LBUTTON_DOWN;
}

IEvent::EventType RMouseUpEv::GetType()
{
	return IEvent::MOUSE_RBUTTON_UP;
}

IEvent::EventType RMouseDownEv::GetType()
{
	return IEvent::MOUSE_RBUTTON_DOWN;
}

