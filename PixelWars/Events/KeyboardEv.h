#pragma once
#include "IEvent.h"


class KeyboardEv : public IEvent
{
public:
	sf::Keyboard::Key keyCode;
	
protected:
	KeyboardEv(sf::Keyboard::Key);
};

class KeyUpEv : public KeyboardEv
{
public:
	KeyUpEv(sf::Keyboard::Key);
	EventType GetType();
};

class KeyDownEv : public KeyboardEv
{
public:
	KeyDownEv(sf::Keyboard::Key);
	EventType GetType();
};