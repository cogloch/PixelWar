#pragma once
#include "IEvent.h"


class FrameEv : public IEvent
{
public:
	FrameEv();
	EventType GetType();
};