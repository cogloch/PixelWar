#pragma once


class IEvent
{
public:
	enum EventType
	{
		// Mouse input
		MOUSE_LBUTTON_UP,
		MOUSE_LBUTTON_DOWN,
		MOUSE_RBUTTON_UP,
		MOUSE_RBUTTON_DOWN,
		
		// Keyboard input
		KEY_UP,
		KEY_DOWN,

		// Camera
		CAMERA_MOVE,
		CAMERA_ENABLE,
		CAMERA_DISABLE,

		// Game session
		GAME_START,
		GAME_OVER,

		// Network events
		NET_JOIN,

		FRAME,

		NUM_EVENTS
	};

	virtual EventType GetType() = 0;
};