#pragma once
#include "Events\IEventListener.h"


class P2PClient : public IEventListener
{
public:
	P2PClient();

	void HandleEvent(IEvent*, IEvent::EventType);

private:
	bool AttemptConnection(const std::string& ip, int port);
};