#include "P2PClient.h"
#include "Events\NetworkEv.h"


P2PClient::P2PClient()
{
	RegisterForEvent(IEvent::NET_JOIN);
}

void P2PClient::HandleEvent(IEvent* pEvent, IEvent::EventType type)
{
	switch (type)
	{
	case IEvent::NET_JOIN:
	{
		NetJoinEv* pNetEv = dynamic_cast<NetJoinEv*>(pEvent);
		AttemptConnection(pNetEv->ip, pNetEv->port);
		break;
	}
	}
}

bool P2PClient::AttemptConnection(const std::string& ip, int port)
{
	return true;
}