#include "Resources\FileManager.h"


Logger::Logger()
{
}

Logger::~Logger()
{
	Close();
}

void Logger::Init()
{
	std::string path = FileManager::GetExeDirectory() + "Log" + ".txt";
	m_logStream.open(path);
	Write("Log file opened.");
}

void Logger::Close()
{
	Write("Log file closed.");
	m_logStream << m_buffer;
	m_logStream.close();
}

void Logger::Write(const std::string& msg, bool writeSeparator)
{
#ifdef _DEBUG
	OutputDebugStringA(msg.c_str());
	OutputDebugStringA("\n");
#endif

	m_buffer += msg + "\n";
	if (writeSeparator)
		WriteSeparator();
}

void Logger::WriteSeparator()
{
	m_buffer += "----------------------------------------------------\n";
}