#pragma once
#include "Game\GameState.h"
#include "Rendering\Renderer.h"


class Application
{
public:
	Application();
	~Application();

	void Init();
	int RunLoop();
	void Close();

private:
	void CopyStateToRenderer();

	sf::RenderWindow m_window;
	Renderer		 m_renderer;
	GameState		 m_gameState;
	
	sf::Event m_event;
	sf::Clock m_clock;

	void OnEvClose();
	void OnEvMousePressed();
	void OnEvMouseReleased();
	void OnEvKeyPresesd();
	void OnEvKeyReleased();
};