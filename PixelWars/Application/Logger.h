#pragma once


class Logger
{
public:
	Logger();
	~Logger();

	void Init();
	void Close();

	void Write(const std::string&, bool writeSeparator = true);
	void WriteSeparator();

private:
	std::string	  m_buffer;
	std::ofstream m_logStream;
};

extern Logger g_logger;