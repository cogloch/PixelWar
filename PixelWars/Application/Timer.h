#pragma once


class Timer
{
public:
	Timer();
	void Update();
	float Elapsed();

private:
	int m_fps;
	int m_maxFps;
	int m_frameCount;
	float m_dt;

	unsigned __int64 m_ticksPerSecond;
	unsigned __int64 m_currentTicks;
	unsigned __int64 m_startupTicks;
	unsigned __int64 m_oneSecTicks;
	unsigned __int64 m_lastTicks;
};