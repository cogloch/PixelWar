#include "Timer.h"


Timer::Timer() : 
	m_fps(0),
	m_maxFps(0),
	m_frameCount(0),
	m_dt(0.0f)
{
	QueryPerformanceFrequency((LARGE_INTEGER*)&m_ticksPerSecond);
	QueryPerformanceCounter((LARGE_INTEGER*)&m_currentTicks);
	m_startupTicks = m_currentTicks;
	m_oneSecTicks = m_currentTicks;
}

float Timer::Elapsed()
{
	return m_dt;
}

void Timer::Update()
{
	m_lastTicks = m_currentTicks;
	QueryPerformanceCounter((LARGE_INTEGER*)&m_currentTicks);

	m_dt = (float)((__int64)m_currentTicks - (__int64)m_lastTicks) / (__int64)m_ticksPerSecond;

	if ((float)((__int64)m_currentTicks - (__int64)m_oneSecTicks) / (__int64)m_ticksPerSecond < 1.0f)
		m_frameCount++;
	else
	{
		m_fps = m_frameCount;

		if (m_fps > m_maxFps)
			m_maxFps = m_fps;

		m_frameCount = 0;
		m_oneSecTicks = m_currentTicks;
	}
}