#include "Application.h"

#include "Events\EventManager.h"
#include "Events\GameSessionEv.h"
#include "Events\FrameEv.h"
#include "Events\KeyboardEv.h"


Application::Application()
{
}

Application::~Application()
{
	Close();
}

void Application::Init()
{
	m_window.create(sf::VideoMode(800, 600), "Space!");
	m_renderer.Init(&m_window);
}

int Application::RunLoop()
{
	while (m_window.isOpen())
	{
		while (m_window.pollEvent(m_event))
		{
			switch (m_event.type)
			{
			case sf::Event::Closed:
				OnEvClose();
				break;

			case sf::Event::MouseButtonPressed:
				OnEvMousePressed();
				break;

			case sf::Event::MouseButtonReleased:
				OnEvMouseReleased();
				break;

			case sf::Event::KeyPressed:
				OnEvKeyPresesd();
				break;

			case sf::Event::KeyReleased:
				OnEvKeyReleased();
				break;
			}
		}

		g_eventManager.PostEvent(new FrameEv());

		// Handle network and input events 
		g_eventManager.DispatchEvents();

		// Perform simulation.  
		//m_gameState.Update();

		// TODO: Signal the other way around
		std::lock_guard<std::mutex> toUpdate(m_renderer.semaphore.mtx);

		if (!m_renderer.semaphore.dataReady)
		{
			// Copy renderable data to the GPU
			//std::lock_guard<std::mutex>(m_renderer.semaphore.mtx);
			CopyStateToRenderer();
			m_renderer.semaphore.dataReady = true;
			m_renderer.semaphore.condVariable.notify_one();
		}
	}

	return 0;
}

void Application::CopyStateToRenderer()
{
	// Camera is always copied
	m_renderer.scene.m_camera = m_gameState.m_camera.view;
	
	// Perform visibility tests
	// Copy resulting data onto render thread
	m_renderer.scene.m_renderables.clear();
	for (auto& ship : m_gameState.m_ships)
	{
		if (TestAABBAABB(m_gameState.m_camera.boundingBox, ship.boundingBox))
		{
			std::vector<Renderable> shipRenderables = ship.GetRenderables();
			m_renderer.scene.m_renderables.insert(m_renderer.scene.m_renderables.end(),
										    	  shipRenderables.begin(), shipRenderables.end());
		}
	}
}

void Application::Close()
{
}

void Application::OnEvClose()
{
	m_window.close();
}

void Application::OnEvMousePressed()
{
	sf::Vector2f pos(static_cast<float>(m_event.mouseButton.x), static_cast<float>(m_event.mouseButton.y));
}

void Application::OnEvMouseReleased()
{
	sf::Vector2f pos(static_cast<float>(m_event.mouseButton.x), static_cast<float>(m_event.mouseButton.y));
	g_eventManager.PostEvent(new GameStartEv());
}

void Application::OnEvKeyPresesd()
{
	g_eventManager.PostEvent(new KeyDownEv(m_event.key.code));
}

void Application::OnEvKeyReleased()
{
	g_eventManager.PostEvent(new KeyUpEv(m_event.key.code));
}