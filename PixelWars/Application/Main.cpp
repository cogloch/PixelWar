#include "Resources\ResourceManager.h"
#include "Resources\FileManager.h"
#include "Events\EventManager.h"
#include "Game\InputManager.h"
#include "Application.h"


Logger			g_logger;
ResourceManager g_resources;
EventManager	g_eventManager;
InputManager	g_inputManager;


int WINAPI wWinMain(HINSTANCE, HINSTANCE, LPWSTR, int)
{
	Application application;
	
	FileManager::Init();

	g_logger.Init();
	g_resources.Init();
	application.Init();

	application.RunLoop();

	application.Close();
	g_eventManager.Close();
	g_logger.Close();

	return 0;
}