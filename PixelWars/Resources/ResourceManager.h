#pragma once


class ResourceManager
{
public:
	ResourceManager();

	bool Init();

	int GetResourceIndexByName(const std::string&);
	int GetTextureByCompType(const std::string&);

	std::vector<sf::Font>	 fonts;
	std::vector<sf::Texture> textures;

private:
	bool LoadAssets();
	bool LoadFonts();
	bool LoadTextures();

	bool LoadComponents();

	// [id] = index of resource(font/texture)
	std::map<std::string, int> m_resourceIds;
	// [component_name] = id of texture
	std::map<std::string, int> m_componentModels;
};

extern ResourceManager g_resources;