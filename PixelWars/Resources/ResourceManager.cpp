#include "FileManager.h"
#include "ResourceManager.h"


ResourceManager::ResourceManager()
{
}

bool ResourceManager::Init()
{
	if (!LoadAssets())
		return false;

	if (!LoadComponents())
		return false;

	return true;
}

int ResourceManager::GetResourceIndexByName(const std::string& name)
{
	return m_resourceIds[name];
}

bool ResourceManager::LoadAssets()
{
	std::string assetDirectory = FileManager::GetAssetDirectory();
	std::ifstream file(assetDirectory + "Assets.cuc");
	if (file.fail())
	{
		g_logger.Write("Failed to open asset descriptor file.");
		return false;
	}

	enum AssetType
	{
		INVALID,
		FONT,
		TEXTURE
	} assetType = INVALID;

	std::map<std::string, AssetType> assetTypeNames = {
		{ "Fonts", FONT },
		{ "Textures", TEXTURE }
	};

	std::string line;
	std::string token;
	unsigned int lineNo = 0;
	
	static std::regex assetNameExpr(R"((?:[^"])[\w\-][\w\d\-]*\.[\w\d]+(?:[^"]))");
	
	while (getline(file, line))
	{
		lineNo++;

		if (!line.length())
			continue;

		if (line[0] == '#')
		{
			token = line.substr(1);

			auto iter = assetTypeNames.find(token);
			if (iter == assetTypeNames.end())
			{
				g_logger.Write("Assets.cuc, line " + std::to_string(lineNo) + ": Invalid asset type: \"" + token + "\".");
				assetType = INVALID;
			}
			else
			{
				assetType = iter->second;
			}
		}
		else
		{
			if (assetType == INVALID)
				continue;

			std::sregex_iterator iter(line.begin(), line.end(), assetNameExpr);
			std::sregex_iterator iter_end;

			while (iter != iter_end)
			{
				token = iter->str();

				switch (assetType)
				{
				case TEXTURE:
				{
					sf::Texture texture;
					if (texture.loadFromFile(assetDirectory + token))
					{
						textures.push_back(texture);
						m_resourceIds[token] = textures.size() - 1;
					}
					else
						g_logger.Write("Assets.cuc, line " + std::to_string(lineNo) + ": Cannot load file: \"" + token + "\".");
					break;
				}
				case FONT:
				{
					sf::Font font;
					if (font.loadFromFile(assetDirectory + token))
					{
						fonts.push_back(font);
						m_resourceIds[token] = fonts.size() - 1;
					}
					else
						g_logger.Write("Assets.cuc, line " + std::to_string(lineNo) + ": Cannot load file: \"" + token + "\".");
					break;
				}
				}

				iter++;
			}
		}
	}

	return true;
}

bool ResourceManager::LoadFonts()
{
	return true;
}

bool ResourceManager::LoadTextures()
{
	return true;
}

bool ResourceManager::LoadComponents()
{
	std::string compDirectory = FileManager::GetAssetDirectory();
	std::ifstream file(compDirectory + "Components.cuc");
	if (file.fail())
	{
		g_logger.Write("Failed to open component descriptor file.");
		return false;
	}

	enum Category
	{
		HULL,
		WEAPON
	} category;

	std::string line;
	std::string contents;
	while (std::getline(file, line))
		contents += line;

	static std::regex exprComponent(R"delim((?:-type = ")(\w+)"(?:-texture = ")(\w+\.\w+))delim");

	enum
	{
		INVALID,

		TYPE,
		TEXTURE
	};

	auto iter = std::sregex_iterator(contents.begin(), contents.end(), exprComponent);
	for (auto end = std::sregex_iterator(); iter != end; iter++)
		m_componentModels[iter->operator[](TYPE)] = m_resourceIds[iter->operator[](TEXTURE)];
}

int ResourceManager::GetTextureByCompType(const std::string& type)
{
	return m_componentModels[type];
}