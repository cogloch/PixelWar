#pragma once


class FileManager
{
public:
	static void Init();

	static std::string GetExeDirectory();
	static std::string GetAssetDirectory();
	static std::string GetShipsDirectory();

private:
	FileManager();
	
	static std::string s_exeDirectory;
	static std::string s_assetDirectory;
	static std::string s_shipsDirectory;
};