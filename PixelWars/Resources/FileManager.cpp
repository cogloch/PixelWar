#include "FileManager.h"


std::string FileManager::s_exeDirectory;
std::string FileManager::s_assetDirectory;
std::string FileManager::s_shipsDirectory;


FileManager::FileManager()
{
}

void FileManager::Init()
{
	char buffer[MAX_PATH];
	GetModuleFileNameA(NULL, buffer, MAX_PATH);

	s_exeDirectory = buffer;
	s_exeDirectory = s_exeDirectory.substr(0, s_exeDirectory.find_last_of("\\") + 1);

	s_assetDirectory = s_exeDirectory + R"(Assets\)";
	s_shipsDirectory = s_exeDirectory + R"(Ships\)";
}

std::string FileManager::GetExeDirectory()
{
	return s_exeDirectory;
}

std::string FileManager::GetAssetDirectory()
{
	return s_assetDirectory;
}

std::string FileManager::GetShipsDirectory()
{
	return s_shipsDirectory;
}