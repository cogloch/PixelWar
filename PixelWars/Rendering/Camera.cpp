#include "Camera.h"
#include "Events\MouseEv.h"
#include "Events\KeyboardEv.h"


Camera::Camera() : m_enabled(false)
{
	RegisterForEvent(IEvent::MOUSE_RBUTTON_DOWN);
	RegisterForEvent(IEvent::MOUSE_RBUTTON_UP);
}

void Camera::HandleEvent(IEvent* pEvent, IEvent::EventType eventType)
{
	switch (eventType)
	{
	case IEvent::MOUSE_RBUTTON_UP:
		break;
	case IEvent::MOUSE_RBUTTON_DOWN:
		break;
	}
}

void Camera::SetEnable(bool enable)
{
	m_enabled = enable;
}

void Camera::Update(float dt)
{
	if (!m_enabled)
		return;


}