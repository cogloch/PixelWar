#include "Renderer.h"
#include "Resources\ResourceManager.h"


Renderer::Renderer()
{
}

Renderer::~Renderer()
{
}

void Renderer::Init(sf::RenderWindow* pWindow)
{
	m_pWindow = pWindow;

	// The OpenGL context can be active in only one thread at a time
	m_pWindow->setActive(false);

	std::thread renderThread(&Renderer::RenderThread, this);
	renderThread.detach();
}

void Renderer::RenderThread()
{
	while (true)
	{
		// Wait for the latest simulation data from main thread
		semaphore.dataReady = false;
		std::unique_lock<std::mutex> lock(semaphore.mtx);
		semaphore.condVariable.wait(lock, [this]{
			return semaphore.dataReady;
		});

		// Render scene with the player camera
		m_pWindow->setView(scene.m_camera);
		RenderScene();

		// Render the UI layer with the 1:1 default camera
		m_pWindow->setView(m_pWindow->getDefaultView());
		RenderUI();
	}
}

void Renderer::Close()
{
}

void Renderer::RenderUI()
{

}

void Renderer::RenderScene()
{
	m_pWindow->clear();

	// Use only one sprite for the entire rendering, since changing textures is just swapping pointers,
	// which is cheaper than storing a texture pointer for each object instance in the game
	sf::Sprite sprite;

	for (const auto& object : scene.m_renderables)
	{
		// Having to pull in the entire resource cache is annoying, but that's the price for not having direct
		// control over binding resources to the pipeline yourself. See above reasoning.
		// TODO: One workaround is to batch like-textures in a single draw call  
		sprite.setTexture(g_resources.textures[object.texture]);
		sprite.setPosition(object.position);
		m_pWindow->draw(sprite);
	}

	m_pWindow->display();
}