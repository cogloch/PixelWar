#pragma once
#include "Events\IEventListener.h"
#include "Physics\AABB.h"


class Camera : public IEventListener
{
public:
	Camera();

	void HandleEvent(IEvent*, IEvent::EventType);
	void SetEnable(bool);
	void Update(float);

	// Used for visibility testing
	AABB boundingBox;
	sf::View view;

private:
	bool m_enabled;

	enum Controls
	{
		UP,
		DOWN,
		LEFT,
		RIGHT,

		NUM_CONTROLS
	};

	sf::Vector2f controlVectors[NUM_CONTROLS];
};