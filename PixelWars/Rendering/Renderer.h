#pragma once
#include "UI.h"
#include "Scene.h"


class Renderer
{
public:
	Renderer();
	~Renderer();

	void Init(sf::RenderWindow*);
	void Close();

	struct Semaphore
	{
		std::mutex mtx;
		std::condition_variable condVariable;
		bool dataReady;
	} semaphore;

	UI	  ui;
	Scene scene;

private:
	void RenderThread();
	void RenderScene();
	void RenderUI();

	sf::RenderWindow* m_pWindow;
};