#pragma once
#include "Renderable.h"
#include "Camera.h"


// Data that the renderer thread will use to draw the space below the UI layer. On each tick, the renderer and the main
// thread will synchronize as to produce a pipelining system. At this point, the main thread will perform visibility 
// testing and will copy only the data within the camera bounds onto the rendering thread, in a Scene instance.
class Scene
{
public:
	// Weird
	// The main thread holds an actual Camera object than can handle events, while Scene, which is meant for a single draw call
	// holds that Camera's.. camera, which is relevant to the renderer. If the Scene would also hold an instance of the Camera 
	// wrapper, then the EventManager from the main thread might fire off an event into the rendering thread, which is off-limits
	// to events, as it would most likely result in a concurrent read(renderer)-write(main). Using locks is also off-limits since
	// the EventManager is already a potential hog that shouldn't accept any more overhead.
	sf::View				m_camera;
	std::vector<Renderable> m_renderables;
};