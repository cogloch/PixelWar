#pragma once


struct Renderable
{
	int texture;
	sf::Vector2f position;
};