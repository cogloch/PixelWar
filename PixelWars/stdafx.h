#pragma once


// Libs
#ifdef SFML_STATIC
#pragma comment(lib, "jpeg.lib")
#pragma comment(lib, "glew.lib")
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "gdi32.lib")  
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "freetype.lib")
#pragma comment(lib, "opengl32.lib")
#endif // SFML_STATIC

// SFML
#include <SFML/Graphics.hpp>

// WinAPI
#include <Windows.h>

// C++
#include <map>
#include <cmath>
#include <string>
#include <vector>
#include <fstream>
#include <unordered_map>

#include <regex>

#include <amp.h>
#include <mutex>
#include <thread>
#include <condition_variable>

#include "Math\Vector2.h"
#include "Application\Logger.h"