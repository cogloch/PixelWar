#pragma once


struct AABB
{
	// min ----------
	// |			|
	// |            |
	// |            |
	// ------------max
	sf::Vector2f min;
	sf::Vector2f max;

	AABB();
	AABB(const sf::Vector2f&, const sf::Vector2f&);
	bool TestAABB(const AABB&);

	void Translate(const sf::Vector2f&);
};

bool TestAABBAABB(const AABB&, const AABB&);