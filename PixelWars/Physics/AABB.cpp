#include "AABB.h"


AABB::AABB() :
	min({ 0, 0 }),
	max({ 0, 0 })
{
}

AABB::AABB(const sf::Vector2f& min_, const sf::Vector2f& max_) :
	min(min_),
	max(max_)
{
}

bool AABB::TestAABB(const AABB& b)
{
	if (max.x < b.min.x || min.x > b.max.x) return false;
	if (max.y < b.min.y || min.y > b.max.y) return false;

	return true;
}

bool TestAABBAABB(const AABB& a, const AABB& b)
{
	if (a.max.x < b.min.x || a.min.x > b.max.x) return false;
	if (a.max.y < b.min.y || a.min.y > b.max.y) return false;

	return true;
}

void AABB::Translate(const sf::Vector2f& delta)
{
	min += delta;
	max += delta;
}