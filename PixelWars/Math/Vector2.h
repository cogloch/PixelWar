#pragma once


template <typename T>
T Magnitude(const sf::Vector2<T>& vec)
{
	return sqrt(vec.x * vec.x + vec.y * vec.y);
}

template <typename T>
T MagnitudeSqr(const sf::Vector2<T>& vec)
{
	return vec.x * vec.x + vec.y * vec.y;
}

